
CREATE SCHEMA groupcc;
CREATE SEQUENCE groupcc.users_id_seq;
CREATE TABLE groupcc.users(
                              id NUMERIC DEFAULT NEXTVAL('groupcc.users_id_seq'),
                              name VARCHAR(100) NOT NULL,
                              surname VARCHAR(100) NOT NULL,
                              email VARCHAR(60) NOT NULL,
                              password VARCHAR(512) NOT NULL,
                              CONSTRAINT users_pk PRIMARY KEY (id),
                              CONSTRAINT users_email_uk UNIQUE (email)
);

CREATE SEQUENCE groupcc.balances_id_seq;
CREATE TABLE groupcc.balances(
                              id NUMERIC DEFAULT NEXTVAL('groupcc.balances_id_seq'),
                              user_id NUMERIC NOT NULL,
                              previous_balance NUMERIC(19,2) DEFAULT 0,
                              current_balance NUMERIC(19,2) DEFAULT 0,
                              created_time TIMESTAMP DEFAULT NOW(),
                              updated_time TIMESTAMP DEFAULT NOW(),
                              currency VARCHAR (5),
                              CONSTRAINT balances_pk PRIMARY KEY (id),
                              CONSTRAINT balances_user_id_fk FOREIGN KEY (user_id) REFERENCES groupcc.users (id)
);

CREATE SEQUENCE groupcc.exchanges_id_seq;
CREATE TABLE groupcc.exchanges(
                                 id NUMERIC DEFAULT NEXTVAL('groupcc.exchanges_id_seq'),
                                 from_currency VARCHAR (5) NOT NULL,
                                 to_currency VARCHAR (5)  NOT NULL,
                                 from_amount NUMERIC(19,2) DEFAULT 0,
                                 to_amount NUMERIC(19,2) DEFAULT 0,
                                 created_time TIMESTAMP DEFAULT NOW(),
                                 user_id NUMERIC NOT NULL,
                                 CONSTRAINT exchanges_pk PRIMARY KEY (id),
                                 CONSTRAINT exchanges_user_id_fk FOREIGN KEY (user_id) REFERENCES groupcc.users (id)
);