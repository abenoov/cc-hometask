const formInputs = document.querySelectorAll('input');
const formButton = document.getElementById('saveProfile');

console.log("formInputs", formInputs);

formButton.addEventListener('click',function(){
    for (let i = 0; i < formInputs.length; i++) {
        formInputs[i].toggleAttribute('readonly');
    };
    if (formButton.innerHTML == "Edit") {
        formButton.innerHTML = "Save";
        formInputs[0].focus();
    } else {
        formButton.innerHTML = "Edit";
    }
});