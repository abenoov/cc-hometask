var loginSectionValue = document.querySelector(".loginInSection");
var signUpSectionValue = document.querySelector(".signUpSection");
var radioBtnValue = document.getElementsByName("radioButton");

document.addEventListener('DOMContentLoaded', function () {
    switchTabs();
});

function switchTabs() {
    for (let radio of radioBtnValue) {
        if (radio.checked) {
            if (radio.value === 'signup') {
                loginSectionValue.style.display = "none";
                signUpSectionValue.style.display = "flex";
            } else {
                loginSectionValue.style.display = "flex";
                signUpSectionValue.style.display = "none";
            }
        }
    }
};
