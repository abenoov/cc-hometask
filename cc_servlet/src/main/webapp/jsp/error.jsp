<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Error request</title>
  <link rel="stylesheet" href="<% out.print(request.getContextPath()); %>/style/all.css"/>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
</head>
<body>
<main class="errorWrapper">
  <section class="errorContainer">
    <i class="bi bi-x-circle"></i>
    <h2><%out.print(request.getAttribute("error-title"));%></h2>
    <h4><%out.print(request.getAttribute("error-message"));%></h4>
    <a href="<% out.print(request.getContextPath()); %>">Go back to the main page</a>
  </section>
</main>
</body>
</html>
