<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<footer>
    <div class="footerContainer">
        <div class="footerQuote">
            <div class="footerLogoWrapper">
                <img class="logoUnipd" src="img/logo-unipd.png" alt="logo unipd"/>
                <img class="companyLogo" src="img/logo.png" alt="logo"/>
            </div>
            <p>
                Through many of its unique properties, Bitcoin allows exciting uses that could not be covered by any previous payment system.
            </p>
        </div>
        <div class="copyright">
            <a href="#">Terms & conditions</a>
            <p>Copyright © CryptoCurrency 2023</p>
        </div>
    </div>
</footer>
