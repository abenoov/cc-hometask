<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Contacts</title>
    <link rel="stylesheet" href="<% out.print(request.getContextPath()); %>/style/all.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
</head>
<body style="display: flex">
<c:import url = "/jsp/sidebar.jsp"/>
<main class="accountContacts">
  <header class="accountHeader">
      <h2>Contacts</h2>
  </header>
  <section class="profileContacts">
    <div class="contactsWrapper">
      <div class="contacts">
        <ul>
          <li>
            <i class="bi bi-telephone-inbound"></i>
            <a href="tel:+396735005">+39 763 5005</a>
          </li>
          <li>
            <i class="bi bi-envelope"></i>
            <a href="mailto:mukhtar.abenov@studenti.unipd.it">
                mukhtar.abenov@studenti.unipd.it
            </a>
          </li>
          <li>
            <i class="bi bi-geo-alt"></i>
            <a target="_blank" href="https://goo.gl/maps/3eVdNceGqkrRH3DZ9">
                Padova PD, Italy
            </a>
          </li>
        </ul>
      </div>
        <form>
          <div class="nameWrapper">
            <div class="formItem">
              <label>Name</label>
              <input placeholder="John" type="text" name="name"/>
            </div>
            <div class="formItem">
              <label>Email</label>
              <input placeholder="example@mail.com" type="email" value="" name="email"/>
            </div>
          </div>
          <div class="formItem">
            <label>Title</label>
            <input placeholder="Title" type="text" name="title"/>
          </div>
            <div class="formItem">
              <label>Subject</label>
              <textarea placeholder="Write something..." name="subject"></textarea>
            </div>
          <div class="formItem">
            <button class="button primaryButton" style="width: 100%">Send message</button>
          </div>
        </form>
    </div>
  </section>
</main>
</body>
</html>