<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form action="user/login/" method="post" class="loginForm">
    <div class="formItem">
        <label>Email Address</label>
        <input name="email" type="email" placeholder="Email" multiple required/>
    </div>
    <div class="formItem">
        <label>Password</label>
        <input name="password" type="password" placeholder="Password" required/>
    </div>
    <div class="formItem">
        <%
            String s = (String) request.getAttribute("signin-message");
            if (s != null) {
                out.print(s);
            }
        %>
    </div>
    <div class="formItem">
        <button class="button primaryButton" type="submit">Log In</button>
    </div>

</form>