<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Profile</title>
    <link rel="stylesheet" href="<% out.print(request.getContextPath()); %>/style/all.css"/>
</head>
<body style="display: flex">
<c:import url = "/jsp/sidebar.jsp"/>
<main class="accountProfile">
    <header class="accountHeader">
        <h2>Profile</h2>
    </header>
    <section class="profileWrapper">
        <div class="profileContainer">
            <form>
                <div class="nameWrapper">
                    <div class="formItem">
                        <label>Name</label>
                        <input type="text" value="John" name="name" readonly/>
                    </div>
                    <div class="formItem">
                        <label>Surname</label>
                        <input type="text" value="Surname" name="surname" readonly/>
                    </div>
                </div>
                <div class="formItem">
                    <label>Telephone number</label>
                    <input type="text" value="+393517635005" name="telNumber" readonly/>
                </div>
                <div class="formItem">
                    <label>Email</label>
                    <input type="email" value="example@studenti.unipd.it" name="email" readonly/>
                </div>
                <div class="nameWrapper">
                    <div class="formItem">
                        <label>Country</label>
                        <input type="text" value="Italy" name="country" readonly/>
                    </div>
                    <div class="formItem">
                        <label>State/Region</label>
                        <input type="text" value="Padova" name="region" readonly/>
                    </div>
                </div>
                <div class="nameWrapper">
                    <div class="formItem">
                        <label>Address</label>
                        <input type="text" value="Via" name="address" readonly/>
                    </div>
                    <div class="formItem">
                        <label>ZIP</label>
                        <input type="number" value="30030" name="zip" readonly/>
                    </div>
                </div>
            </form>
            <div class="formItem">
                <button id="saveProfile" class="button primaryButton" style="width: 100%">Edit</button>
            </div>
        </div>
    </section>
</main>
<script src="js/profile.js"></script>
</body>
</html>
