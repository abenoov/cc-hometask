<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<header class="mainHeader">
  <div class="navContaienr">
      <a class="logo" href="#">
          <img src="img/logo.png" alt="logo" />
      </a>
      <button onclick="redirectAuth()" class="button primaryButton">Get started</button>
  </div>
</header>

<script language="JavaScript">
    function redirectAuth(){
      location.href = '<% out.print(request.getContextPath()); %>/authorization';
    }
</script>