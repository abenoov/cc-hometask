<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Wallet</title>
    <link rel="stylesheet" href="<% out.print(request.getContextPath()); %>/style/all.css"/>
</head>
<body style="display: flex">
<c:import url="/jsp/sidebar.jsp"/>
<main class="accountMain">
    <header class="accountHeader">
        <h2>My wallet</h2>
        <h3>Balance: EUR — 0</h3>
    </header>
    <section class="balanceWrapper">
        <div class="checkoutFormContainer">
            <h3>Recharge wallet</h3>
            <form method="post" action="<%out.print(request.getContextPath());%>/balance/topup">
                <div class="formItem">
                    <label>Telephone number:</label>
                    <input type="text" name="email" placeholder="+395005050"/>
                </div>
                <div class="formItem">
                    <label>Price:</label>
                    <input type="number" name="amount" placeholder="3000"/>
                </div>
                <div class="formItem">
                    <button class="button primaryButton" style="width: 100%" type="submit">Pay
                    </button>
                </div>
            </form>
        </div>
        <div class="infoCoinCointainer">
            <p>info coin conitaner</p>
        </div>

    </section>

</main>
</body>
</html>
