<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html>
<head>
    <title>Authorization</title>
    <link rel="stylesheet" href="<% out.print(request.getContextPath()); %>/style/all.css"/>
</head>
<body style="background: #0B8552">
<section class="authWrapper">
    <div class="formContainer">
        <div class="selectWrapper">
            <div class="radioBtnContainer">
                <input
                        type="radio"
                        id="logIn"
                        name="radioButton"
                        value="login"
                        checked
                        onclick="switchTabs()"
                >
                <label>Log In</label>
            </div>
            <div class="radioBtnContainer">
                <input
                        type="radio"
                        id="signUp"
                        name="radioButton"
                        value="signup"
                        onclick="switchTabs()"
                >
                <label>Sign Up</label>
            </div>
        </div>
        <div class="loginInSection">
            <c:import url="/jsp/logIn.jsp"/>
        </div>
        <div class="signUpSection">
            <c:import url="/jsp/signUp.jsp"/>
        </div>
    </div>
    <div class="authLogoContainer">
        <a href="welcome.jsp.jsp">
            <img src="<% out.print(request.getContextPath()); %>/img/logo.png" alt="logo"/>
        </a>
        <p>By clicking next, I hereby acknowledge that I agree to the terms of the
            CryptoCurrency.</p>
    </div>
</section>
<script src="<% out.print(request.getContextPath()); %>/js/authorization.js"></script>

<script language="JavaScript">

  var action = '<% out.print(request.getAttribute("auth-action"));%>';
  if (action !== 'signin' && action !== 'signup') {
    action = 'signin';
  }

  if (action === 'signup') {
    document.getElementById('signUp').checked = true;
    switchTabs();
  }

</script>

</body>
</html>
