<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Home</title>
    <link rel="stylesheet" href="<% out.print(request.getContextPath()); %>/style/all.css"/>
</head>
<body>
<%--Header component--%>
<c:import url="/jsp/header.jsp"/>
<main class="main">
    <section class="section1">
        <div class="welcomeText">
            <span>Join to our crypto exchange</span>
            <h1>Popular way to buy and sell crypto.</h1>
            <p>
                CryptoCurrency is the community-run technology powering the cryptocurrency, ether
                and thousands of decentralized applications.
            </p>
        </div>
        <div class="bannerWrapper">
            <img src="img/banner.png" alt="banner"/>
        </div>
    </section>
    <section class="section2">
        <div class="bannerSection2">
            <img src="img/banner-section2.png" alt="banner section2"/>
        </div>
        <div class="exploreText">
            <span>Your gateway to the global economy</span>
            <h2>CryptoCurrency lending, trading, and custody for worldwide</h2>
            <p>
                Institutional Markets is a full-stack crypto services platform that works worldwide
                with crypto-native businesses and institutional clients on trading, and custody
                solutions.
            </p>
            <button class="button primaryButton">Get it now</button>
        </div>
    </section>
    <section class="section3">
        <div class="exploreText">
            <h2>We make crypto easy</h2>
            <p>
                Specific cryptocurrencies work and get a bit of crypto to try out for yourself. Here
                are a few reasons why you should choose CryptoCurrency.
            </p>
            <button class="button primaryButton">Learn more</button>
        </div>
        <div class="featuresWrapper">

            <div class="feature">
                <img src="img/cloud-data-icon.svg" alt="option1 icon"/>
                <div class="featureText">
                    <h4>Secure storage</h4>
                    <p>We store the vast majority of the digital assets in secure offline
                        storage.</p>
                </div>
            </div>
            <div class="feature">
                <img src="img/user-icon.svg" alt="option2 icon"/>
                <div class="featureText">
                    <h4>Protected by insurance</h4>
                    <p>Cryptocurrency stored on our servers is covered by our insurance policy.</p>
                </div>
            </div>
            <div class="feature">
                <img src="img/award-icon.svg" alt="option3 icon"/>
                <div class="featureText">
                    <h4>Industry best practices</h4>
                    <p>CryptoCurrency supports a variety of the most popular digital crypto
                        currencies.</p>
                </div>
            </div>
            <div class="feature">
                <img src="img/trade-icon.svg" alt="option4 icon"/>
                <div class="featureText">
                    <h4>Trade Assets</h4>
                    <p>Discover new and innovative crypto assets with over 200 spot trading pairs
                        and 25 margin.</p>
                </div>
            </div>
        </div>
    </section>
</main>
<%--Footer component--%>
<c:import url="/jsp/footer.jsp"/>

<script src="js/authorization.js"></script>
</body>
</html>