<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form action="" method="post" class="exchangeForm">
    <label>From:</label>
    <div class="formItem">
        <select name="currencies">
            <option value="btc">BTC</option>
            <option value="eth">ETH</option>
            <option value="usdt">USDT</option>
            <option value="bnb">BNB</option>
        </select>
        <input name="fromPrice" type="number" multiple required/>
    </div>
        <label>To:</label>
    <div class="formItem">
        <select name="currencies">
            <option value="eur">EUR</option>
        </select>
        <input style="width: 100%" name="toPrice" type="number" required/>
    </div>
    <div class="formItem">
        <button class="button primaryButton" style="width: 100%" type="submit">Exchange</button>
    </div>
</form>
