<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form action="user/sign-up/" method="post" class="loginForm">
    <div class="nameWrapper">
        <div class="formItem">
            <label>Name</label>
            <input name="name" type="text" placeholder="Name" required/>
        </div>
        <div class="formItem">
            <label>Surname</label>
            <input name="surname" type="text" placeholder="Surname" required/>
        </div>
    </div>
    <div class="formItem">
        <label>Email</label>
        <input name="email" type="email" placeholder="Email" multiple required/>
    </div>
    <div class="formItem">
        <label>Password</label>
        <input name="password" type="password" placeholder="Password" required/>
    </div>
    <div class="formItem">
        <%
            String s = (String) request.getAttribute("signup-message");
            if (s != null) {
                out.print(s);
            }
        %>
    </div>
    <div class="formItem">
        <button class="button primaryButton" type="submit">Sign Up</button>
    </div>
</form>