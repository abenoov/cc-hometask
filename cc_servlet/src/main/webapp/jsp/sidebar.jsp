<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<aside class="sideBar">
    <nav>
        <ul>
            <li>
                <a href="<%=request.getContextPath()%>/jsp/wallet.jsp">My wallet</a>
            </li>
            <li>
                <a href="<%=request.getContextPath()%>/jsp/exchange.jsp">Exchange</a>
            </li>
            <li>
                <a href="<%=request.getContextPath()%>/jsp/profile.jsp">Profile</a>
            </li>
            <li>
                <a href="<%=request.getContextPath()%>/jsp/contacts.jsp">Contacts</a>
            </li>
            <li>
                <a href="<%=request.getContextPath()%>/user/logout/">Logout</a>
            </li>
        </ul>
    </nav>
</aside>