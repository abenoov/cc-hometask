<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Exchange</title>
  <link rel="stylesheet" href="<% out.print(request.getContextPath()); %>/style/all.css"/>
</head>
<body style="display: flex">
  <c:import url = "/jsp/sidebar.jsp"/>
<main class="accountMain">
  <header class="accountHeader">
    <h2>Exchange</h2>
    <h3>Balance: EUR — 0</h3>
  </header>
  <section class="exchangeWrapper">
    <div class="exchangeContainer">
      <h3>Exchange</h3>
      <c:import url = "/jsp/exchangeForm.jsp"/>
    </div>
    <div class="transactionsContainer">
      <c:import url = "/jsp/transactions.jsp"/>
    </div>
  </section>
</main>
</body>
</html>
