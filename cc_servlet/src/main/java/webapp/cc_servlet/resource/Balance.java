package webapp.cc_servlet.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class Balance extends AbstractResource {

  private Long id;
  private BigDecimal currentBalance;
  private BigDecimal previousBalance;
  private LocalDateTime createdTime;
  private LocalDateTime updatedTime;
  private Long userId;
  private String currency;

  public Balance() {
  }

  public Balance(
      Long id,
      BigDecimal currentBalance,
      BigDecimal previousBalance,
      LocalDateTime createdTime,
      LocalDateTime updatedTime,
      Long userId,
      String currency) {
    this.id = id;
    this.currentBalance = currentBalance;
    this.previousBalance = previousBalance;
    this.createdTime = createdTime;
    this.updatedTime = updatedTime;
    this.userId = userId;
    this.currency = currency;
  }

  public static void toJSON(OutputStream out, List<Balance> balances) throws IOException {
    final JsonGenerator jg = JSON_FACTORY.createGenerator(out);
    jg.writeStartArray();

    for (Balance balance : balances) {
      jg.writeStartObject();
      jg.writeStringField("id", getStringValue(balance.id));
      jg.writeStringField("currentBalance", getStringValue(balance.currentBalance));
      jg.writeStringField("previousBalance", getStringValue(balance.previousBalance));
      jg.writeStringField("createdAt", getStringValue(balance.createdTime));
      jg.writeStringField("updatedAt", getStringValue(balance.updatedTime));
      jg.writeStringField("userId", getStringValue(balance.userId));
      jg.writeStringField("currency", getStringValue(balance.currency));
      jg.writeEndObject();
    }

    jg.writeEndArray();
    jg.flush();
  }

  @Override
  protected void writeJSON(OutputStream out) throws Exception {
    final JsonGenerator jg = JSON_FACTORY.createGenerator(out);
    jg.writeStartObject();
    jg.writeFieldName("balance");
    jg.writeStartObject();
    jg.writeStringField("id", getStringValue(this.id));
    jg.writeStringField("currentBalance", getStringValue(this.currentBalance));
    jg.writeStringField("previousBalance", getStringValue(this.previousBalance));
    jg.writeStringField("createdAt", getStringValue(this.createdTime));
    jg.writeStringField("updatedAt", getStringValue(this.updatedTime));
    jg.writeStringField("userId", getStringValue(this.userId));
    jg.writeStringField("currency", getStringValue(this.currency));
    jg.writeEndObject();
    jg.writeEndObject();
    jg.flush();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public BigDecimal getCurrentBalance() {
    return currentBalance;
  }

  public void setCurrentBalance(BigDecimal currentBalance) {
    this.currentBalance = currentBalance;
  }

  public BigDecimal getPreviousBalance() {
    return previousBalance;
  }

  public void setPreviousBalance(BigDecimal previousBalance) {
    this.previousBalance = previousBalance;
  }

  public LocalDateTime getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(LocalDateTime createdTime) {
    this.createdTime = createdTime;
  }

  public LocalDateTime getUpdatedTime() {
    return updatedTime;
  }

  public void setUpdatedTime(LocalDateTime updatedTime) {
    this.updatedTime = updatedTime;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }
}
