package webapp.cc_servlet.resource;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;

public abstract class AbstractResource implements Resource, Serializable {

  /**
   * A LOGGER available for all the subclasses.
   */
  protected static final Logger LOGGER = LogManager
      .getLogger(AbstractResource.class, StringFormatterMessageFactory.INSTANCE);

  /**
   * The JSON factory to be used for creating JSON parsers and generators.
   */
  protected static final JsonFactory JSON_FACTORY;

  static {
    // set up the JSON factory
    JSON_FACTORY = new JsonFactory();
    JSON_FACTORY.disable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
    JSON_FACTORY.disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);

    LOGGER.debug("JSON factory successfully setup.");
  }

  @Override
  public void toJSON(final OutputStream out) throws IOException {

    if (out == null) {
      LOGGER.error("The output stream cannot be null.");
      throw new IOException("The output stream cannot be null.");
    }

    try {
      writeJSON(out);
    } catch (Exception e) {
      LOGGER.error("Unable to serialize the resource to JSON.", e);
      throw new IOException("Unable to serialize the resource to JSON.", e);
    }

  }

  /**
   * Performs the actual writing of JSON.
   * <p>
   * Subclasses have to implement this method to provide the actual logic needed for representing
   * the {@code Resource} to JSON.
   *
   * @param out the stream to which the JSON representation of the {@code Resource} has to be
   *            written.
   * @throws Exception if something goes wrong during writing.
   */
  protected abstract void writeJSON(OutputStream out) throws Exception;

  protected static String getStringValue(String v) {
    return v != null ? v : null;
  }

  protected static String getStringValue(Long v) {
    return v != null ? v.toString() : null;
  }

  protected static String getStringValue(LocalDateTime v) {
    return v != null ? v.format(DateTimeFormatter.ISO_DATE_TIME) : null;
  }

  protected static String getStringValue(BigDecimal v) {
    return v != null ? v.toString() : null;
  }
}
