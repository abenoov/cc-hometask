package webapp.cc_servlet.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Exchange extends AbstractResource {

  private Long id;
  private String fromCurrency;
  private String toCurrency;
  private BigDecimal fromAmount;
  private BigDecimal toAmount;
  private LocalDateTime createAt;
  private Long userId;

  public Exchange() {
  }

  public Exchange(Long id, String fromCurrency, String toCurrency, BigDecimal fromAmount,
      BigDecimal toAmount, LocalDateTime createAt,
      Long userId) {
    this.id = id;
    this.fromCurrency = fromCurrency;
    this.toCurrency = toCurrency;
    this.fromAmount = fromAmount;
    this.toAmount = toAmount;
    this.createAt = createAt;
    this.userId = userId;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFromCurrency() {
    return fromCurrency;
  }

  public void setFromCurrency(String fromCurrency) {
    this.fromCurrency = fromCurrency;
  }

  public String getToCurrency() {
    return toCurrency;
  }

  public void setToCurrency(String toCurrency) {
    this.toCurrency = toCurrency;
  }

  public BigDecimal getFromAmount() {
    return fromAmount;
  }

  public void setFromAmount(BigDecimal fromAmount) {
    this.fromAmount = fromAmount;
  }

  public BigDecimal getToAmount() {
    return toAmount;
  }

  public void setToAmount(BigDecimal toAmount) {
    this.toAmount = toAmount;
  }

  public LocalDateTime getCreateAt() {
    return createAt;
  }

  public void setCreateAt(LocalDateTime createAt) {
    this.createAt = createAt;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public static void toJSON(OutputStream out, List<Exchange> exchanges) throws IOException {
    final JsonGenerator jg = JSON_FACTORY.createGenerator(out);
    jg.writeStartArray();

    for (Exchange exchange : exchanges) {
      jg.writeStartObject();
      jg.writeStringField("id", getStringValue(exchange.getId()));
      jg.writeStringField("fromCurrency", getStringValue(exchange.getFromCurrency()));
      jg.writeStringField("toCurrency", getStringValue(exchange.getToCurrency()));
      jg.writeStringField("fromAmount", getStringValue(exchange.getFromAmount()));
      jg.writeStringField("toAmount", getStringValue(exchange.getToAmount()));
      jg.writeStringField("userId", getStringValue(exchange.getUserId()));
      jg.writeStringField("createdAt", getStringValue(exchange.getCreateAt()));
      jg.writeEndObject();
    }

    jg.writeEndArray();
    jg.flush();
  }

  @Override
  protected void writeJSON(OutputStream out) throws Exception {
    final JsonGenerator jg = JSON_FACTORY.createGenerator(out);
    jg.writeStartObject();
    jg.writeFieldName("exchange");
    jg.writeStartObject();
    jg.writeStringField("id", getStringValue(this.getId()));
    jg.writeStringField("fromCurrency", getStringValue(this.getFromCurrency()));
    jg.writeStringField("toCurrency", getStringValue(this.getToCurrency()));
    jg.writeStringField("fromAmount", getStringValue(this.getFromAmount()));
    jg.writeStringField("toAmount", getStringValue(this.toAmount));
    jg.writeStringField("userId", getStringValue(this.getUserId()));
    jg.writeStringField("createdAt", getStringValue(this.getCreateAt()));
    jg.writeEndObject();
    jg.writeEndObject();
    jg.flush();
  }

  public static Exchange fromJSON(final InputStream in) throws IOException {

    // the fields read from JSON
    String jId = null;
    String jFromCurrency = null;
    String jToCurrency = null;
    String jFromAmount = null;
    String jToAmount = null;
    String jCreatedAt = null;
    String jUserId = null;

    try {
      final JsonParser jp = JSON_FACTORY.createParser(in);

      while (jp.nextToken() != JsonToken.END_OBJECT) {

        if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

          switch (jp.getCurrentName()) {
            case "id":
              jp.nextToken();
              jId = jp.getText();
              break;
            case "fromCurrency":
              jp.nextToken();
              jFromCurrency = jp.getText();
              break;
            case "toCurrency":
              jp.nextToken();
              jToCurrency = jp.getText();
              break;
            case "fromAmount":
              jp.nextToken();
              jFromAmount = jp.getText();
              break;
            case "toAmount":
              jp.nextToken();
              jToAmount = jp.getText();
              break;
            case "userId":
              jp.nextToken();
              jUserId = jp.getText();
              break;
            case "createdAt":
              jp.nextToken();
              jCreatedAt = jp.getText();
              break;
          }
        }
      }
    } catch (IOException e) {
      LOGGER.error("Unable to parse an User object from JSON.", e);
      throw e;
    }

    return new Exchange(
        jId != null ? Long.parseLong(jId) : null,
        jFromCurrency,
        jToCurrency,
        jFromAmount != null ? new BigDecimal(jFromAmount) : null,
        jToAmount != null ? new BigDecimal(jToAmount) : null,
        jCreatedAt != null ? LocalDateTime.parse(jCreatedAt, DateTimeFormatter.ISO_DATE_TIME)
            : null,
        jUserId != null ? Long.parseLong(jUserId) : null
    );
  }
}
