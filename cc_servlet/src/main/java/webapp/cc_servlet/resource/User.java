package webapp.cc_servlet.resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class User extends AbstractResource {

  private Long id;

  private String name;

  private String surname;

  private String email;

  private String password;

  // Used in registration
  public User(Long id, String name, String surname, String email, String password) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.password = password;
  }


  // Used in registration
  public User(String name, String surname, String email, String password) {
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.password = password;
  }

  // Used in login
  public User(final String email, final String password) {
    this.email = email;
    this.password = password;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  protected final void writeJSON(final OutputStream out) throws IOException {

    final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

    jg.writeStartObject();

    jg.writeFieldName("user");

    jg.writeStartObject();

    jg.writeStringField("name", name);

    jg.writeStringField("surname", surname);

    jg.writeStringField("email", email);

    jg.writeStringField("password", password);

    jg.writeEndObject();

    jg.writeEndObject();

    jg.flush();
  }

  public static User fromJSON(final InputStream in) throws IOException {

    // the fields read from JSON
    String jName = null;
    String jSurname = null;
    String jEmail = null;
    String jPassword = null;

    try {
      final JsonParser jp = JSON_FACTORY.createParser(in);

      // while we are not on the start of an element or the element is not
      // a token element, advance to the next element (if any)
      while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !"user".equals(jp.getCurrentName())) {

        // there are no more events
        if (jp.nextToken() == null) {
          LOGGER.error("No User object found in the stream.");
          throw new EOFException("Unable to parse JSON: no User object found.");
        }
      }

      while (jp.nextToken() != JsonToken.END_OBJECT) {

        if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

          switch (jp.getCurrentName()) {
            case "name":
              jp.nextToken();
              jName = jp.getText();
              break;
            case "surname":
              jp.nextToken();
              jSurname = jp.getText();
              break;
            case "email":
              jp.nextToken();
              jEmail = jp.getText();
              break;
            case "password":
              jp.nextToken();
              jPassword = jp.getText();
              break;
          }
        }
      }
    } catch (IOException e) {
      LOGGER.error("Unable to parse an User object from JSON.", e);
      throw e;
    }

    return new User(jName, jSurname, jEmail, jPassword);
  }
}
