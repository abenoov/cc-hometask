package webapp.cc_servlet.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import webapp.cc_servlet.dao.BalancesDAO;
import webapp.cc_servlet.dao.GetUserByEmailDAO;
import webapp.cc_servlet.resource.Balance;
import webapp.cc_servlet.resource.User;

public class BalanceServlet extends AbstractDatabaseServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    //take the request uri
    LogContext.setIPAddress(req.getRemoteAddr());
    LogContext.setResource(req.getRequestURI());
    LogContext.setAction("BALANCE");
    String op = req.getRequestURI();
    LOGGER.info("op {}", op);

    User user = getCurrentUser(req);
    if (user == null) {
      sendError(req, resp, new IllegalAccessError("Access denied"));
      return;
    }

    if (op.endsWith("/balance")) {
      try {
        getBalances(req, resp, user);
      } catch (SQLException throwable) {
        sendError(req, resp, throwable);
      }
      return;
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    //take the request uri
    LogContext.setIPAddress(req.getRemoteAddr());
    LogContext.setResource(req.getRequestURI());
    LogContext.setAction("BALANCE");
    String op = req.getRequestURI();
    LOGGER.info("op {}", op);

    User user = getCurrentUser(req);
    if (user == null) {
      sendError(req, resp, new IllegalAccessError("Access denied"));
      return;
    }

    if (op.endsWith("/balance/topup")) {
      try {
        topupEurBalance(req, resp, user);
      } catch (SQLException throwable) {
        sendError(req, resp, throwable);
      }
      return;
    }
  }

  private void topupEurBalance(HttpServletRequest req, HttpServletResponse resp, User user)
      throws SQLException, ServletException, IOException {
    String currency = "EUR";
    String email = req.getParameter("email");
    String amount = req.getParameter("amount");

    try (Connection connection = getConnection()) {
      User topupUser = new User(email, null);
      topupUser = new GetUserByEmailDAO(connection, topupUser).access().getOutputParam();
      if (topupUser == null) {
        sendError(req, resp, new IllegalArgumentException("User not found"));
        return;
      }
      List<Balance> balances = new BalancesDAO(connection,
          user.getId()).getBalances();
      Balance eurBalance = null;
      for (Balance b : balances) {
        if (currency.equals(b.getCurrency())) {
          eurBalance = b;
          break;
        }
      }

      if (eurBalance == null) {
        eurBalance = new Balance();
        eurBalance.setPreviousBalance(BigDecimal.ZERO);
        eurBalance.setCurrentBalance(new BigDecimal(amount));
        eurBalance.setUserId(topupUser.getId());
        eurBalance.setCurrency(currency);
        eurBalance.setUpdatedTime(LocalDateTime.now());
        eurBalance.setCreatedTime(LocalDateTime.now());

        new BalancesDAO(connection, topupUser.getId())
            .create(eurBalance);
      } else {
        eurBalance.setPreviousBalance(eurBalance.getCurrentBalance());
        eurBalance.setCurrentBalance(eurBalance.getCurrentBalance().add(new BigDecimal(amount)));
        new BalancesDAO(connection, topupUser.getId())
            .update(eurBalance);
      }
      redirect(req, resp, "/jsp/wallet.jsp");
    }
  }

  private void getBalances(
      HttpServletRequest req,
      HttpServletResponse resp,
      User user) throws SQLException, IOException {
    try (Connection connection = getConnection()) {

      List<Balance> balances = new BalancesDAO(connection,
          user.getId()).getBalances();

      try (OutputStream stream = resp.getOutputStream()) {
        Balance.toJSON(stream, balances);
      }
    }
  }
}
