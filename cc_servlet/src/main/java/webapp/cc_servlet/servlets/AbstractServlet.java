package webapp.cc_servlet.servlets;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.StringFormatterMessageFactory;
import webapp.cc_servlet.resource.User;

public abstract class AbstractServlet extends HttpServlet {

  /**
   * A LOGGER available for all the subclasses.
   */
  protected static final Logger LOGGER = LogManager.getLogger(AbstractDatabaseServlet.class,
      StringFormatterMessageFactory.INSTANCE);

  public void redirect(HttpServletRequest req, HttpServletResponse res, String destination)
      throws ServletException, IOException {
    RequestDispatcher requestDispatcher = req.getRequestDispatcher(destination);
    requestDispatcher.forward(req, res);
  }

  public User getCurrentUser(HttpServletRequest request) {
    HttpSession session = request.getSession();
    if (session == null) {
      return null;
    }
    Object user = session.getAttribute("user");
    return user != null ? (User) user : null;
  }

  public void sendError(
      HttpServletRequest request,
      HttpServletResponse response,
      Throwable throwable) throws IOException, ServletException {
    response.setStatus(500);
    request.setAttribute("error-title", "something wrong");
    request.setAttribute("error-message", throwable.getMessage());
    redirect(request, response, "/jsp/error.jsp");
  }

}
