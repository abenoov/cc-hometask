package webapp.cc_servlet.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import webapp.cc_servlet.dao.CreateUserDAO;
import webapp.cc_servlet.dao.GetUserByEmailDAO;
import webapp.cc_servlet.dao.UserLoginDAO;
import webapp.cc_servlet.resource.Message;
import webapp.cc_servlet.resource.User;

@WebServlet(name = "UserServlet", value = "/user/*")
public final class UserServlet extends AbstractDatabaseServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    doPost(req, resp);
  }

  @Override
  public void doPost(HttpServletRequest req, HttpServletResponse res)
      throws ServletException, IOException {
    //take the request uri
    LogContext.setIPAddress(req.getRemoteAddr());
    LogContext.setResource(req.getRequestURI());
    LogContext.setAction("LOGIN");
    String op = req.getRequestURI();

    //remove everything prior to /STUDENT/ (included) and use the remainder as
    //indicator for the required operation
    op = op.substring(op.lastIndexOf("user") + 5);
    LOGGER.info("op {}", op);

    switch (op) {
      case "login/":
        req.setAttribute("auth-action", "signin");
        // the requested operation is login
        loginOperations(req, res, false);
        break;
      case "sign-up/":
        req.setAttribute("auth-action", "signup");
        // the requested operation is register
        registrationOperations(req, res);
        break;
      case "logout/":
        // the requested operation is register
        closeSession(req, res);
        res.sendRedirect(req.getContextPath());
        break;
      default:
        Message m = new Message("An error occurred default", "E200", "Operation Unknown");
        LOGGER.error("stacktrace {}:", m.getMessage());
    }
  }

  public void loginOperations(HttpServletRequest req, HttpServletResponse res, boolean isValid)
      throws ServletException, IOException {

    User user = null;
    Message m = null;

    String regex_psw = "^(?=.*[A-Z])(?=.*[0-9]).{8,}$";
    String regex_email = "^[a-z0-9+_.-]+@[a-z0-9.-]+\\.[a-z]{2,}$";

    try (
        Connection connection = getConnection();
    ) {
      String email = req.getParameter("email");
      String password = req.getParameter("password");
      LOGGER.info("User {} is trying to login", email);

      if (isValid) {
        email = email.toLowerCase();
        LOGGER.info("email to lower {}", email);
        User u = new User(email, password);

        // try to find the user in the database
        user = new UserLoginDAO(connection, u)
            .access()
            .getOutputParam();
        LOGGER.info("email to lower2 {}", user.getEmail());
        //the UserDAO will tell us if the email exists and the password
        //matches
        if (user == null) {
          //if not, tell it to the user
          m = new Message("The user does not exist", "E200", "Missing user");
          LOGGER.error("problems with user: {}", m.getMessage());
        } else {
          m = null;
          LOGGER.info("THE USER {} LOGGED IN", user.getEmail());
          createSession(req, res, user);
          redirect(req, res, "/jsp/wallet.jsp");
          return;
        }
      } else {
        if (email == null || email.equals("")) {
          //the email is null (was not set on the parameters) or an empty string
          //notify this to the user
          m = new Message("Insert an email", "E200", "Missing fields");
          LOGGER.error("problems with fields: {}", m.getMessage());
        } else if (password == null || password.equals("")) {
          //the password was empty
          m = new Message("Insert the password", "E200", "Missing fields");
          LOGGER.error("problems with fields: {}", m.getMessage());
        }
        // check password is compliant
        else if (!password.matches(regex_psw)) {
          m = new Message("This password is not compliant", "E200", "Password not compliant");

          LOGGER.error("problems with fields: {}", m.getMessage());
        }
        // check email is compliant
        else if (!email.matches(regex_email)) {
          m = new Message("This is not an email", "E200", "Email not compliant");
          LOGGER.error("problems with fields: {}", m.getMessage());
        } else {
          //try to authenticate the user
          email = email.toLowerCase();
          LOGGER.info("email to lower {}", email);
          User u = new User(email, password);
          // try to find the user in the database
          user = new UserLoginDAO(connection, u)
              .access()
              .getOutputParam();
          if (user == null) {
            //if not, tell it to the user
            m = new Message("The user does not exist", "E200", "Missing user");
            LOGGER.error("problems with user: {}", m.getMessage());
          } else {
            m = null;
            LOGGER.info("THE USER {} LOGGED IN", user.getEmail());
            createSession(req, res, user);
            redirect(req, res, "/jsp/wallet.jsp");
            return;
          }
        }
      }
    } catch (SQLException e) {
      m = new Message("An error occurred in SQL", "E200", e.getMessage());
      LOGGER.error("stacktrace:", e);
    } catch (NumberFormatException e) {
      m = new Message("An error occurred handling numbers", "E200", e.getMessage());
      LOGGER.error("stacktrace:", e);
    } finally {
      if (m != null) {
        req.setAttribute("signin-message", m.getMessage());
        redirect(req, res, "/jsp/authorization.jsp");
        return;
      }
    }
  }


  public void registrationOperations(HttpServletRequest req, HttpServletResponse res)
      throws IOException, ServletException {
    // request parameters
    String name;
    String surname;
    String email;
    String password;

    // model
    User user = null;
    Message m = null;

    try (Connection connection = getConnection()) {

      //get the registration parameters
      name = req.getParameter("name");
      surname = req.getParameter("surname");
      email = req.getParameter("email");
      password = req.getParameter("password");

      LOGGER.info("user {} is trying to register", email);
      // regex to validate email and password
      String regex_psw = "^(?=.*[A-Z])(?=.*[0-9]).{8,}$";
      String regex_email = "^[a-z0-9+_.-]+@[a-z0-9.-]+\\.[a-z]{2,}$";

      //check that all registrations parameters have been set and are not null
      if (name == null || name.equals("") ||
          surname == null || surname.equals("") ||
          email == null || email.equals("") ||
          password == null || password.equals("")) {

        m = new Message("Some fields are empty", "E200", "Missing fields");

        LOGGER.error("problems with fields: {}", m.getMessage());
      }
      // check password is compliant
      else if (!password.matches(regex_psw)) {
        m = new Message("This password is not compliant", "E200", "Password not compliant");

        LOGGER.error("problems with fields: {}", m.getMessage());
      }
      // check email is compliant
      else if (!email.matches(regex_email)) {
        m = new Message("This is not an email", "E200", "Email not compliant");

        LOGGER.error("problems with fields: {}", m.getMessage());
      } else {
        // all the parameters are correct
        // initialize a new User
        user = new User(name, surname, email, password);

        // check if the user already exists
        boolean userExists = new GetUserByEmailDAO(connection, user)
            .access()
            .getOutputParam() != null;
        if (userExists) {
          // the user with this email is already signed up
          m = new Message("The user with this email is already signed up", "E200",
              "User already exists");
          LOGGER.error("problems with fields: {}", m.getMessage());
        } else {
          // create a new user resource
          new CreateUserDAO(connection, user)
              .access()
              .getOutputParam();

          LOGGER.info("User %d successfully created in the database.", email);

          //if the registration ended correctly, forward the user to the
          //login service: note that, now the login service will login the user
          //and create the session. We are not redirecting the user to the
          //login page
          loginOperations(req, res, true);
          m = null;
          return;
        }
      }
    } catch (SQLException | ServletException e) {
      m = new Message("An error occurred SQL, SERVLET", "E200", e.getMessage());
      LOGGER.error("stacktrace:", e);
    } catch (NumberFormatException e) {
      m = new Message("An error occurred handling numbers", "E200", e.getMessage());
      LOGGER.error("stacktrace:", e);
    } finally {
      if (m != null) {
        req.setAttribute("signup-message", m.getMessage());
        redirect(req, res, "/jsp/authorization.jsp");
        return;
      }
    }

  }


  private void createSession(
      HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse,
      User user) {
    HttpSession session = httpServletRequest.getSession(true);
    session.setAttribute("user", user);
  }

  private void closeSession(
      HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse) {
    HttpSession session = httpServletRequest.getSession();
    if (session != null) {
      session.invalidate();
    }
  }
}
