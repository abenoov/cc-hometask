package webapp.cc_servlet.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import webapp.cc_servlet.dao.BalancesDAO;
import webapp.cc_servlet.dao.ExchangeDAO;
import webapp.cc_servlet.resource.Balance;
import webapp.cc_servlet.resource.Exchange;
import webapp.cc_servlet.resource.User;

public class ExchangeServlet extends AbstractDatabaseServlet {


  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    LogContext.setIPAddress(req.getRemoteAddr());
    LogContext.setResource(req.getRequestURI());
    LogContext.setAction("EXCHANGE");
    String op = req.getRequestURI();
    LOGGER.info("op {}", op);

    User user = getCurrentUser(req);
    if (user == null) {
      sendError(req, resp, new IllegalAccessError("Access denied"));
      return;
    }

    Exchange nExchange = Exchange.fromJSON(req.getInputStream());

    try (Connection connection = getConnection()) {
      List<Balance> balances = new BalancesDAO(
          connection, user.getId()).getBalances();

      Balance from = getBalance(user.getId(), balances, nExchange.getFromCurrency());
      Balance to = getBalance(user.getId(), balances, nExchange.getToCurrency());
      if (from.getCurrentBalance().compareTo(nExchange.getFromAmount()) < 0) {
        sendError(req, resp, new IllegalArgumentException("Not sufficient fund"));
        return;
      }

      from.setPreviousBalance(from.getCurrentBalance());
      from.setCurrentBalance(from.getCurrentBalance().subtract(nExchange.getFromAmount()));

      to.setPreviousBalance(to.getCurrentBalance());
      to.setCurrentBalance(to.getCurrentBalance().add(nExchange.getToAmount()));

      if (from.getId() > 0L) {
        new BalancesDAO(connection, user.getId()).update(from);
      } else {
        new BalancesDAO(connection, user.getId()).create(from);
      }

      if (to.getId() > 0L) {
        new BalancesDAO(connection, user.getId()).update(to);
      } else {
        new BalancesDAO(connection, user.getId()).create(to);
      }

      nExchange.setUserId(user.getId());
      nExchange.setCreateAt(LocalDateTime.now());
      new ExchangeDAO(connection, user.getId()).create(nExchange);

    } catch (SQLException throwable) {
      sendError(req, resp, throwable);
      return;
    }
  }

  private Balance getBalance(Long userId, List<Balance> balances, String currency) {
    for (Balance balance : balances) {
      if (currency.equals(balance.getCurrency())) {
        return balance;
      }
    }
    Balance balance = new Balance();
    balance.setId(-1L);
    balance.setCurrentBalance(BigDecimal.ZERO);
    balance.setPreviousBalance(BigDecimal.ZERO);
    balance.setCreatedTime(LocalDateTime.now());
    balance.setUpdatedTime(LocalDateTime.now());
    balance.setUserId(userId);
    return balance;
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    LogContext.setIPAddress(req.getRemoteAddr());
    LogContext.setResource(req.getRequestURI());
    LogContext.setAction("EXCHANGE");
    String op = req.getRequestURI();
    LOGGER.info("op {}", op);

    User user = getCurrentUser(req);
    if (user == null) {
      sendError(req, resp, new IllegalAccessError("Access denied"));
      return;
    }

    try {
      getExchanges(req, resp, user);
    } catch (SQLException throwable) {
      sendError(req, resp, throwable);
    }
    return;

  }

  private void getExchanges(HttpServletRequest req, HttpServletResponse resp, User user)
      throws SQLException, IOException {
    try (Connection connection = getConnection()) {

      List<Exchange> exchanges = new ExchangeDAO(connection,
          user.getId()).getExchanges();

      try (OutputStream stream = resp.getOutputStream()) {
        Exchange.toJSON(stream, exchanges);
      }
    }
  }
}
