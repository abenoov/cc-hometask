package webapp.cc_servlet.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;
import webapp.cc_servlet.resource.User;

@WebServlet(name = "HelloServlet", value = "")
public class HelloServlet extends AbstractServlet {

  public void doGet(HttpServletRequest req, HttpServletResponse res)
      throws IOException, ServletException {

    HttpSession session = req.getSession();
    if (session == null) {
      redirect(req, res, "/welcome");
      return;
    }
    User user = (User) session.getAttribute("user");
    if (Objects.isNull(user)) {
      redirect(req, res, "/welcome");
    } else {
      redirect(req, res, "/wallet");
    }
  }
}