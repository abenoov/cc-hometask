package webapp.cc_servlet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import webapp.cc_servlet.resource.User;

public final class CreateUserDAO extends AbstractDAO<User> {

  /**
   * The SQL statement to be executed
   */
  private static final String STATEMENT = "INSERT "
      + "INTO groupcc.users (name, surname, email, password) "
      + "VALUES (?, ?, ?, ?) RETURNING *";

  private final User user;

  public CreateUserDAO(final Connection con, final User user) {
    super(con);
    if (user == null) {
      LOGGER.error("The user cannot be null.");
      throw new NullPointerException("The user cannot be null.");
    }

    this.user = user;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateUserDAO that = (CreateUserDAO) o;
    return Objects.equals(user, that.user);
  }

  @Override
  public int hashCode() {
    return Objects.hash(user);
  }

  @Override
  protected final void doAccess() throws SQLException {
    try (
        PreparedStatement pstmt = con.prepareStatement(STATEMENT)) {

      pstmt.setString(1, user.getName());
      pstmt.setString(2, user.getSurname());
      pstmt.setString(3, user.getEmail());
      pstmt.setString(4, user.getPassword());

      processStatement(pstmt);
    }

  }

  private void processStatement(PreparedStatement pstmt) throws SQLException {
    User u;
    try (ResultSet rs = pstmt.executeQuery()) {
      if (rs.next()) {
        u = new User(
            rs.getString("name"),
            rs.getString("surname"),
            rs.getString("email"),
            rs.getString("password"));

        LOGGER.info("User %d successfully stored in the database.", u.getEmail());
        outputParam = u;
      }
    }
  }

}
