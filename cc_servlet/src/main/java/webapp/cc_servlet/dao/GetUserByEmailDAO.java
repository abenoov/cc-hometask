package webapp.cc_servlet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import webapp.cc_servlet.resource.User;

public class GetUserByEmailDAO extends AbstractDAO<User> {

  /**
   * The SQL statement to be executed
   */
  private static final String STATEMENT = """
      SELECT 
      id,
      name,
      surname,
      email 
      FROM groupcc.users 
      WHERE email = ?
      """;

  private final User user;

  public GetUserByEmailDAO(final Connection con, final User user) {
    super(con);

    if (user == null) {
      LOGGER.error("The user cannot be null.");
      throw new NullPointerException("The user cannot be null.");
    }
    this.user = user;
  }

  @Override
  protected final void doAccess() throws SQLException {
    try (PreparedStatement pstmt = con.prepareStatement(STATEMENT);) {
      pstmt.setString(1, user.getEmail());
      executeStatement(pstmt);
    }
  }

  private User executeStatement(PreparedStatement pstmt) throws SQLException {
    try (ResultSet rs = pstmt.executeQuery()) {
      if (rs.next()) {
        LOGGER.info("User found {}.", user.getEmail());
        this.outputParam = new User(
            rs.getLong("id"),
            rs.getString("name"),
            rs.getString("surname"),
            rs.getString("email"),
            null
        );
      } else {
        LOGGER.info("User NOT found {}.", user.getEmail());
        this.outputParam = null;
      }
    }
    return this.outputParam;
  }
}
