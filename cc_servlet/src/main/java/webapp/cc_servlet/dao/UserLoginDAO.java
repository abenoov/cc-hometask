package webapp.cc_servlet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import webapp.cc_servlet.resource.User;

public class UserLoginDAO extends AbstractDAO<User> {

  private static final String STATEMENT_LOGIN = "SELECT id, name, surname, email FROM groupcc.users WHERE email = ? AND password = ?";

  private final User user;

  public UserLoginDAO(final Connection con, final User user) {
    super(con);
    this.user = user;
  }

  @Override
  protected void doAccess() throws Exception {
    try (PreparedStatement stmnt = con.prepareStatement(STATEMENT_LOGIN);) {
      stmnt.setString(1, user.getEmail());
      stmnt.setString(2, user.getPassword());
      executeStatement(stmnt);
    }
  }

  private void executeStatement(PreparedStatement stmnt) throws SQLException {
    try (ResultSet rs = stmnt.executeQuery()) {
      if (rs.next()) {
        User user0
            = new User(rs.getLong(1),
            rs.getString(2),
            rs.getString(3),
            rs.getString(4),
            user.getPassword());
        this.outputParam = user0;
        LOGGER.info("User logged in {}.", user0.getEmail());
      } else {
        LOGGER.error("Error logging in the user {}", user.getEmail());
      }

    }
  }
}
