package webapp.cc_servlet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import webapp.cc_servlet.resource.Exchange;

public class ExchangeDAO extends AbstractDAO<Exchange> {


  private final String SQL_GET = """
      SELECT 
      id, 
      from_currency, 
      to_currency, 
      from_amount, 
      to_amount, 
      created_time, 
      user_id
      FROM groupcc.exchanges
      WHERE user_id = ?
      """;

  public final String CREATE_EXCHANGE = """
      INSERT INTO groupcc.exchanges(
      	from_currency, 
      	to_currency, 
      	from_amount, 
      	to_amount, 
      	user_id)
      	VALUES (?, ?, ?, ?, ?);
      """;

  private final Long userId;

  /**
   * Creates a new DAO object.
   *
   * @param con the connection to be used for accessing the database.
   */
  public ExchangeDAO(Connection con, Long userId) {
    super(con);
    this.userId = userId;
  }

  @Override
  protected void doAccess() throws Exception {
  }

  public List<Exchange> getExchanges() throws SQLException {
    try (PreparedStatement stmt = con.prepareStatement(SQL_GET)) {
      stmt.setLong(1, userId);
      return executeStatement(stmt);
    }
  }

  private List<Exchange> executeStatement(PreparedStatement stmnt) throws SQLException {
    ArrayList<Exchange> exchanges = new ArrayList<>();
    try (ResultSet rs = stmnt.executeQuery()) {
      while (rs.next()) {
        final Exchange exchange
            = new Exchange(
            rs.getLong(1),
            rs.getString(2),
            rs.getString(3),
            rs.getBigDecimal(4),
            rs.getBigDecimal(5),
            getLocalDateTime(rs, 6),
            rs.getLong(7)
        );
        exchanges.add(exchange);
      }
    }
    return exchanges;
  }

  private LocalDateTime getLocalDateTime(ResultSet rs, int i) throws SQLException {
    return LocalDateTime.from(
        Instant.ofEpochMilli(
            rs.getTimestamp(i)
                .getTime())
            .atZone(
                ZoneId.systemDefault()));
  }

  public void create(Exchange balance) throws SQLException {
    try (PreparedStatement statement = con.prepareStatement(CREATE_EXCHANGE)) {
      statement.setString(1, balance.getFromCurrency());
      statement.setString(2, balance.getToCurrency());
      statement.setBigDecimal(3, balance.getFromAmount());
      statement.setBigDecimal(4, balance.getToAmount());
      statement.setLong(5, balance.getUserId());
      statement.execute();
    }
  }


}
