package webapp.cc_servlet.dao;

import java.sql.SQLException;

public interface DataAccessObject<T> {
    DataAccessObject<T> access() throws SQLException;

    T getOutputParam();
}
