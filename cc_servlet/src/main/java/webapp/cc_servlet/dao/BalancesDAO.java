package webapp.cc_servlet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import webapp.cc_servlet.resource.Balance;

public class BalancesDAO extends AbstractDAO<Balance> {


  private final String SQL_GET = """
      SELECT 
      id, 
      user_id, 
      previous_balance, 
      current_balance, 
      created_time, 
      updated_time,
      currency 
      FROM groupcc.balances 
      WHERE user_id = ?
      """;

  public final String CREATE_BALANCE = """
      INSERT INTO groupcc.balances(
        user_id, previous_balance, current_balance, currency)
        VALUES (?, ?, ?, ?);
      """;

  public final String UPDATE_BALANCE = """
      UPDATE groupcc.balances
      	SET previous_balance=?, current_balance=?, updated_time=NOW()
      	WHERE id = ?;
      """;

  private final Long userId;

  /**
   * Creates a new DAO object.
   *
   * @param con the connection to be used for accessing the database.
   */
  public BalancesDAO(Connection con, Long userId) {
    super(con);
    this.userId = userId;
  }

  @Override
  protected void doAccess() throws Exception {
  }

  public List<Balance> getBalances() throws SQLException {
    try (PreparedStatement stmt = con.prepareStatement(SQL_GET)) {
      stmt.setLong(1, userId);
      return executeStatement(stmt);
    }
  }

  private List<Balance> executeStatement(PreparedStatement stmnt) throws SQLException {
    ArrayList<Balance> balances = new ArrayList<>();
    try (ResultSet rs = stmnt.executeQuery()) {
      while (rs.next()) {
        final Balance balance
            = new Balance(
            rs.getLong(1),
            rs.getBigDecimal(4),
            rs.getBigDecimal(3),
            getLocalDateTime(rs, 5),
            getLocalDateTime(rs, 6),
            rs.getLong(2),
            rs.getString(7)
        );
        balances.add(balance);
      }
    }
    return balances;
  }

  private LocalDateTime getLocalDateTime(ResultSet rs, int i) throws SQLException {
    return LocalDateTime.from(
        Instant.ofEpochMilli(
            rs.getTimestamp(i)
                .getTime())
            .atZone(
                ZoneId.systemDefault()));
  }

  public void create(Balance balance) throws SQLException {
    try (PreparedStatement statement = con.prepareStatement(CREATE_BALANCE)) {
      statement.setLong(1, balance.getUserId());
      statement.setBigDecimal(2, balance.getPreviousBalance());
      statement.setBigDecimal(3, balance.getCurrentBalance());
      statement.setString(4, balance.getCurrency());

      statement.execute();

    }
  }

  public void update(Balance balance) throws SQLException {
    try (PreparedStatement statement = con.prepareStatement(UPDATE_BALANCE)) {
      statement.setBigDecimal(1, balance.getPreviousBalance());
      statement.setBigDecimal(2, balance.getCurrentBalance());
      statement.setLong(3, balance.getId());

      statement.execute();
    }
  }

}
